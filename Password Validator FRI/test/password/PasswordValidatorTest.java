package password;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * 
 * @author troya  991-530-754
 *
 */
public class PasswordValidatorTest {

	@Test
	public void testIsValidLengthRegular() {
		//fail("Invalid password length");
		boolean isValidLength = PasswordValidator.isValidLength("Validpassword");
		assertTrue("Invalid Password Length", isValidLength);
	}
	
	
	@Test
	public void testIsValidSpaceRegular() {
		//fail("Invalid password length");
		boolean passLength = PasswordValidator.isValidLength("Valid Pass");
		assertFalse("Invalid Password Length", passLength);
	}
	
	

	@Test
	public void testIsValidLengthException() {
	//fail("Invalid password length");
     boolean isValidLength = PasswordValidator.isValidLength("Valid");
	assertFalse("Invalid Password Length", isValidLength);
	}
	
	

	
	
	@Test
	public void testIsValidLengthExceptionSpace() {
		//fail("Invalid password length");
		boolean passLength = PasswordValidator.isValidLength("Valid Pass");
		assertFalse("Invalid Password Length", passLength);
	}
	
	
	/**
	@Test
	public void testIsValidLengthExceptionNull() {
		//fail("Invalid password length");
		boolean passLength = PasswordValidator.isValidLength(null);
		assertFalse("Invalid Password Length", passLength);
	}
	*/
	
	
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		//fail("Invalid password length");
	     boolean passLength = PasswordValidator.isValidLength("Validpa");
		assertFalse("Invalid Password Length", passLength);
	}
	
	@Test
	public void testIsValidSpaceBoundaryOut() {
		//fail("Invalid password length");
	     boolean passLength = PasswordValidator.isValidLength("Validpa ");
		assertFalse("Invalid Password Length", passLength);
	}

	
	
	
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		//fail("Invalid password length");
		boolean passLength = PasswordValidator.isValidLength("Validpas");
		assertTrue("Invalid Password Length", passLength);
	}
	
	
	
	@Test
	public void testIsValidSpaceBoundaryIn() {
		//fail("Invalid password length");
		boolean passLength = PasswordValidator.isValidLength("Validpas");
		assertTrue("Invalid Password Length", passLength);
	}
	

}
